package com.pengpeng.mobileinfo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.Service;
import android.content.Context;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.text.format.Formatter;

/**
 * 这里是工具类
 * 
 * @author Li Hongjun
 */
public class Utils {
	private static String imei = null;
	private static String imsi = null;

	/**
	 * 获取手机IMEI 2010-5-6
	 * 
	 * @author Li Hongjun
	 */
	public static String getIMEI(Context c) {
		if (imei == null || imei.equals("")) {
			try {
				TelephonyManager tm = (TelephonyManager) c
						.getSystemService(Service.TELEPHONY_SERVICE);
				try {
					// 酷派手机专用获取IMEI号
					Class<?> su = Class
							.forName("com.yulong.android.server.systeminterface.util.SystemUtil");
					Class<?> ptypes[] = new Class[1];
					ptypes[0] = Class.forName("android.content.Context");
					Method method = su.getMethod("getIMEI", ptypes);
					imei = (String) method.invoke(null, c);
				} catch (Exception e) {
					imei = tm.getDeviceId();
					// imei="&customid=4000071";
					// imei="862020980392117";
				}
				// imei = "000000000000001";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return imei;
	}

	/**
	 * 获取sim卡串号 2010-5-10
	 * 
	 * @author Li Hongjun
	 */
	public static String getIMSI(Context c) {
		if (imsi == null || imsi.equals("")) {
			try {
				TelephonyManager tm = (TelephonyManager) c
						.getSystemService(Service.TELEPHONY_SERVICE);
				imsi = tm.getSimSerialNumber();
				// imsi="000000000000000&customerid=1010101212";

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return imsi;
	}

	/**
	 * 获取手机号 2010-5-6
	 * 
	 * @author Li Hongjun
	 */
	public static String getPhoneNumber(Context c) {
		try {
			TelephonyManager tm = (TelephonyManager) c
					.getSystemService(Service.TELEPHONY_SERVICE);
			return tm.getLine1Number();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取媒体音量 2010-5-27
	 * 
	 * @author Li Hongjun
	 */
	public static int getMusicVolume(Context c) {
		try {
			AudioManager aManager = (AudioManager) c
					.getSystemService(Context.AUDIO_SERVICE);
			return aManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * 获取系统固件版本,如1.5 2010-5-6
	 * 
	 * @author Li Hongjun
	 */
	public static String getOSVersion() {
		// 2.1_update1太长,服务端存储太小,暂时改用SDK号
		return Build.VERSION.SDK;
		// return Build.VERSION.RELEASE;
	}

	/**
	 * 获取api版本,如1.5对应的3 2010-5-6
	 * 
	 * @author Li Hongjun
	 */
	public static String getSDKVersion() {
		return Build.VERSION.SDK;
	}

	/**
	 * 获取手机型号 2010-5-6
	 * 
	 * @author Li Hongjun
	 */
	public static String getMobileModel() {
		return Build.MODEL.replaceAll(" ", "");
	}

	/**
	 * 构造app版本信息 2010-5-10
	 * 
	 * @author Li Hongjun
	 */
	public static String getAppVersion() {
		return "Android" + getSDKVersion() + "V5";
	}

	/**
	 * 判断是否装载的SDcard 2010-4-14
	 * 
	 * @author Li Hongjun
	 */
	public static boolean isSDCard() {
		if (Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * sd卡是否还有剩余空间
	 * 
	 * @author Li Hongjun
	 */
	public static boolean isSDCardFree() {
		if (isSDCard()) {
			File path = Environment.getExternalStorageDirectory();
			// 取得sdcard文件路径
			StatFs statfs = new StatFs(path.getPath());
			// 获取block的SIZE
			long blocSize = statfs.getBlockSize();
			// 获取BLOCK数量
			// long totalBlocks=statfs.getBlockCount();
			// 可使用的Block的数量
			long availaBlock = statfs.getAvailableBlocks();
			if (availaBlock * blocSize > 1024 * 1024 * 10) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 返回20100416010101格式的时间 2010-4-16
	 * 
	 * @author Li Hongjun
	 */
	public static String returnNowTime() {
		Date date = new Date();
		String timeString = "";
		try {
			timeString = DateFormat.format("yyyyMMddkkmmss", date).toString();
		} catch (Exception e) {
			LogInfo.LogOut("returnNowTime excepiton:" + e.getMessage());
		}
		return timeString;
	}

	/**
	 * 返回yyyy_MM_dd_kk_mm格式的当前时间
	 */
	public static String returnNowTimeFormat() {
		Date date = new Date();
		String timeString = "";
		try {
			timeString = DateFormat.format("yyyy_MM_dd_kk_mm", date).toString();
		} catch (Exception e) {
			LogInfo.LogOut("returnNowTime excepiton:" + e.getMessage());
		}
		return timeString;
	}

	/**
	 * 将1970格式的时间转换成可读时间yyyyMMddkkmmss 2010-5-25
	 * 
	 * @author Li Hongjun
	 */
	public static long return1970_to_Time(long time) {
		Date date = new Date(time);
		long timeT = time;
		try {
			timeT = Long.parseLong(DateFormat.format("yyyyMMddkkmmss", date)
					.toString());
		} catch (Exception e) {
			LogInfo.LogOut("return1970_to_Time excepiton:" + e.getMessage());
		}
		return timeT;
	}

	/**
	 * 对time1和time2进行比较,如果time1在time2之前,则为true 2010-4-20
	 * 
	 * @author Li Hongjun
	 */
	public static boolean returnCompare(String time1, String time2) {
		boolean rs = false;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date1 = sdf.parse(time1);
			Date date2 = sdf.parse(time2);
			return date1.getTime() < date2.getTime();
		} catch (Exception e) {
			LogInfo.LogOut("returnCompare excepiton:" + e.getMessage());
		}
		return rs;
	}

	/**
	 * 对yyyy-MM-dd格式的日期,计算两个日期之间的天数 2010-11-2
	 * 
	 * @author Li Hongjun
	 */
	public static int returnCalculateDays(String fromDay, String toDay) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date1 = sdf.parse(fromDay);
			Date date2 = sdf.parse(toDay);
			return (int) ((date2.getTime() - date1.getTime()) / 1000 / 60 / 60 / 24);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * 对时间进行更改,将time 调整secds秒钟,secds为正时向后延迟,secds为负时向前提前 2010-4-20
	 * 
	 * @author Li Hongjun
	 */
	public static String returnChangeTime(String time, int secds) {
		String timeString = time;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = sdf.parse(time);
			date.setTime(date.getTime() + secds * 1000);
			timeString = sdf.format(date);
			// timeString=DateFormat.format("yyyyMMddkkmmss", date).toString();
		} catch (Exception e) {
			LogInfo.LogOut("returnChangeTime excepiton:" + e.getMessage());
		}
		return timeString;
	}

	/**
	 * 删除文件 2010-4-19
	 * 
	 * @author Li Hongjun
	 */
	public static void deleteFile(String fileAllPathAndName) {
		try {
			if (fileAllPathAndName == null) {
				return;
			}
			File file = new File(fileAllPathAndName);
			if (file.exists()) {
				file.delete();
			}
		} catch (Exception e) {
			e.printStackTrace();
			LogInfo.LogOut(e.getMessage());
		}
	}

	/**
	 * 判断音频文件是否存在 2010-4-29
	 * 
	 * @author Li Hongjun
	 */
	public static boolean isFileExist(String path) {
		if (path == null) {
			return false;
		}
		File file = new File(path);
		return file.exists();
	}

	/**
	 * 判断是否是mac格式文件
	 * 
	 * @author Li Hongjun
	 */
	public static boolean isMAC(String path) {
		if (path == null) {
			return false;
		} else {
			return path.toLowerCase().endsWith("mac");
		}
	}

	/**
	 * 判断是否是amr格式文件
	 * 
	 * @author Li Hongjun
	 */
	public static boolean isAMR(String path) {
		if (path == null) {
			return false;
		} else {
			return path.toLowerCase().endsWith("amr");
		}
	}

	/**
	 * 判断是否是mp3格式文件
	 * 
	 * @author Li Hongjun
	 */
	public static boolean isMP3(String path) {
		if (path == null) {
			return false;
		} else {
			return path.toLowerCase().endsWith("mp3");
		}
	}

	/**
	 * 生成文件下载临时地址
	 * 
	 * @author Li Hongjun
	 */
	public static String buildFinalFileTempPathDown(String realPath) {
		return realPath.substring(0, realPath.lastIndexOf('.')) + ".tmp";
	}

	/**
	 * 生成文件播放临时地址
	 * 
	 * @author Li Hongjun
	 */
	public static String buildFinalFileTempPathListen(String realPath) {
		return realPath.substring(0, realPath.lastIndexOf('.')) + ".tmp2";
	}

	/**
	 * 生成文件类型 2010-5-10
	 * 
	 * @author Li Hongjun
	 */
	public static String buildFileType(String fileUrl) {
		String type = null;
		try {
			if (fileUrl != null && fileUrl.split("\\.").length > 0) {
				type = fileUrl.split("\\.")[fileUrl.split("\\.").length - 1];
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return type;
	}

	/**
	 * 获取已经下载的文件长度,用与断点续传 2010-4-28
	 * 
	 * @author Li Hongjun
	 */
	public static long getTempFileLength(String path) {
		if (path == null) {
			return 0;
		} else {
			File file = new File(path);
			if (file.exists()) {
				return file.length();
			} else {
				return 0;
			}
		}
	}

	/**
	 * 删除下载临时文件 2010-4-19
	 * 
	 * @author Li Hongjun
	 */
	public static void deleteTempFile(String path) {
		if (path == null) {
			return;
		} else {
			File file = new File(path);
			if (file.exists()) {
				file.delete();
			}
		}
	}

	/**
	 * 格式化秒数 如36000秒-->10:00:00 2010-5-10
	 * 
	 * @author Li Hongjun
	 */
	public static String formatSeconds(long secs) {
		String string = "00:00";
		try {
			String tString;
			if (secs > 60 * 60) {
				tString = String.valueOf(secs / 60 / 60);
				tString = tString.length() > 1 ? tString : "0" + tString;
				string = tString + ":";// 小时

				tString = String.valueOf(secs % 3600 / 60);
				tString = tString.length() > 1 ? tString : "0" + tString;
				string = string + (tString) + ":";// 分钟

				tString = String.valueOf(secs % 60);
				tString = tString.length() > 1 ? tString : "0" + tString;
				string = string + tString;
			} else {
				tString = String.valueOf(secs / 60);
				tString = tString.length() > 1 ? tString : "0" + tString;
				string = tString + ":";

				tString = String.valueOf(secs % 60);
				tString = tString.length() > 1 ? tString : "0" + tString;
				string = string + tString;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return string;
	}

	/**
	 * 格式化秒数 如10:00:00.000-->36000 毫秒数去掉 2010-5-10
	 * 
	 * @author Li Hongjun
	 */
	public static long formatSeconds(String secs) {
		long time = 0;
		try {
			if (secs == null) {
				return 0;
			}
			secs = secs.split("\\.")[0];
			String s[] = secs.split(":");
			if (s.length > 3 || s.length < 1) {
				return 0;
			} else {
				time = s.length == 3 ? Long.parseLong(s[s.length - 3]) * 60 * 60
						: time;// 小时
				time = s.length > 1 ? time + Long.parseLong(s[s.length - 2])
						* 60 : time;// 分钟
				time = time + Long.parseLong(s[s.length - 1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return time;
	}

	/**
	 * 获取本机语言类型 2010-4-22
	 * 
	 * @author Li Hongjun
	 */
	public static String getLocal() {
		Locale locale = Locale.getDefault();
		// ISO-639 2字母编码格式
		// 英文 en
		// 日文 ja
		// 简体中文 zh
		String langCode = locale.getLanguage();

		return langCode;
	}

	public static void getMEM(Activity activity) {
		LogInfo.LogOut("totalMemory=" + Runtime.getRuntime().totalMemory());
		LogInfo.LogOut("freeMemory=" + Runtime.getRuntime().freeMemory());
		ActivityManager am = (ActivityManager) activity
				.getSystemService(Context.ACTIVITY_SERVICE);
		MemoryInfo mi = new MemoryInfo();
		am.getMemoryInfo(mi);
		LogInfo.LogOut("availMem="
				+ Formatter.formatFileSize(activity.getApplicationContext(),
						mi.availMem));
		LogInfo.LogOut("lowMemory=" + mi.lowMemory);
		LogInfo.LogOut("getAvailableInternalMemorySize="
				+ formatSize(getAvailableInternalMemorySize()));
		LogInfo.LogOut("getTotalInternalMemorySize="
				+ formatSize(getTotalInternalMemorySize()));
	}

	static public boolean externalMemoryAvailable() {
		return android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED);
	}

	static public long getAvailableInternalMemorySize() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return availableBlocks * blockSize;
	}

	static public long getTotalInternalMemorySize() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long totalBlocks = stat.getBlockCount();
		return totalBlocks * blockSize;
	}

	static public String formatSize(long size) {
		String suffix = null;

		if (size >= 1024) {
			suffix = "KiB";
			size /= 1024;
			if (size >= 1024) {
				suffix = "MiB";
				size /= 1024;
			}
		}
		StringBuilder resultBuffer = new StringBuilder(Long.toString(size));
		int commaOffset = resultBuffer.length() - 3;
		while (commaOffset > 0) {
			resultBuffer.insert(commaOffset, ',');
			commaOffset -= 3;
		}
		if (suffix != null)
			resultBuffer.append(suffix);
		return resultBuffer.toString();
	}


	public static boolean isNetworkValidate(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getActiveNetworkInfo() != null) {
			return cm.getActiveNetworkInfo().isAvailable();
		}
		return false;
	}
	
	public static String getCurrentTime() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date curDate = new Date(System.currentTimeMillis());
		return formatter.format(curDate);
	}
	public static int getCurrentYEAR() {
		Date curDate = new Date(System.currentTimeMillis());
		return curDate.getYear();
	}
	public static int getCurrentMONTH() {
		Date curDate = new Date(System.currentTimeMillis());
		
		return curDate.getMonth();
	}
	public static int getCurrentDAY() {
		Date curDate = new Date(System.currentTimeMillis());
		
		return curDate.getDate();
	}
	
	public static byte[] getData(InputStream in){
		if (in == null) {
			return null;
		}
		ByteArrayOutputStream bs = new ByteArrayOutputStream();
		byte[] b = new byte[1024];
		int len = 0;
		try {
			while((len = in.read(b, 0, b.length)) != -1) {
				bs.write(b, 0, len);
			}
			return bs.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getLocalMacAddress(Context context) {
		String re = "";
		try {
			WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);   
			WifiInfo info = wifi.getConnectionInfo();
			re = info.getMacAddress().replace(":", "");
			int len = 15 - re.length();
			if (len > 0) {
				String para = System.currentTimeMillis() + "";
				re = re + para.substring(para.length() - len, para.length());
			} else {
				re.substring(0, 15);
			}
		} catch (Exception e) {
			re = System.currentTimeMillis() + getRandom();
		}
        return re;
    }  
	
	public static String getRandom() {
		int temp = (int)(100 * Math.random());
		while(temp == 0 || temp == 100) {
			temp = (int)(100 * Math.random());
		}
		return temp + "";
	}
}
