package com.pengpeng.mobileinfo;

import java.io.File;
import java.io.InputStream;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.text.ClipboardManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;

public class Application extends Activity implements OnClickListener {
	TextView t;
	Button copy, send, menu;
	boolean isPort = true;

	String portParams = null;
	String landParams = null;
	View main;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MobclickAgent.setDebugMode(true);// 设置debug 默认true
		MobclickAgent.setSessionContinueMillis(60000);// 设置判断启动次数的间隔时间,毫秒,默认30000
		MobclickAgent.setAutoLocation(false);// 是否手机位置信息,默认true
		// MobclickAgent.onKillProcess(this);//杀死进程前调用

		getLayoutInflater();
		main = LayoutInflater.from(this).inflate(R.layout.main, null);
		// main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		setContentView(main);
		t = (TextView) findViewById(R.id.text);
		t.postDelayed(new Runnable() {
			@Override
			public void run() {
				getInfo();
			}
		}, 100);
		copy = (Button) findViewById(R.id.button1);
		copy.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				emulateShiftHeld();
			}
		});
		send = (Button) findViewById(R.id.button2);
		send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (portParams == null || landParams == null) {
					Toast.makeText(Application.this, "请转屏后再发送邮件", Toast.LENGTH_SHORT).show();
				} else {
					EmailUtils.sendEmail(Application.this, new String[] { "xu.hengyu@audiocn.com", "li.hongjun@audiocn.com" }, "目标适配PAD参数", portParams + "\n\n" + landParams);
				}

			}
		});
		menu = (Button) findViewById(R.id.button3);
		menu.setOnClickListener(this);
		menu.setVisibility(View.GONE);
	}

	private void getInfo() {
		if (getResources().getConfiguration().orientation == android.content.res.Configuration.ORIENTATION_LANDSCAPE) {
			isPort = false;
		} else {
			isPort = true;
		}
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		StringBuffer buffer = new StringBuffer();

		buffer.append("\n" + "当前为" + (isPort == true ? "竖" : "横") + "屏参数");
		buffer.append("\n" + "================BASE==========");
		buffer.append("\n" + "Width=" + wm.getDefaultDisplay().getWidth() + "  Height=" + wm.getDefaultDisplay().getHeight());
		buffer.append("\n" + "density=" + metrics.density + "  densityDpi=" + metrics.densityDpi + "  " + metrics.toString());
		buffer.append("\n" + "IMEI=" + Utils.getIMEI(this) + "\nDeviceid=" + getDeviceId());
		buffer.append("\n" + "IMSI=" + Utils.getIMSI(this));
		buffer.append("\n" + "MODEL=" + Utils.getMobileModel());
		buffer.append("\n" + "BOARD=" + Build.BOARD);
		buffer.append("\n" + "BRAND=" + Build.BRAND);
		buffer.append("\n" + "DISPLAY=" + Build.DISPLAY);
		buffer.append("\n" + "PRODUCT=" + Build.PRODUCT);
		buffer.append("\n" + "DEVICE=" + Build.DEVICE);
		buffer.append("\n" + "FINGERPRINT=" + Build.FINGERPRINT);
		buffer.append("\n" + "HARDWARE=" + Build.HARDWARE);
		buffer.append("\n" + "HOST=" + Build.HOST);
		buffer.append("\n" + "Local=" + Utils.getLocal());
		buffer.append("\n" + "IsNetWork=" + Utils.isNetworkValidate(this));
		buffer.append("\n" + "IsSDcard=" + Utils.isSDCard());
		buffer.append("\n" + "SDcardPath=" + Environment.getExternalStorageDirectory().getAbsolutePath());
		buffer.append("\n" + "Version=Android" + Build.VERSION.RELEASE + "\t versionCode=" + Build.VERSION.SDK_INT);
		buffer.append("\n" + "statusHeight=" + getStatusHeight());
		buffer.append("\n" + "titleHeight=" + getTitleBarHeight());
		buffer.append("\n" + "Time=" + Utils.returnNowTimeFormat());
		buffer.append("\n" + "================Version==========");
		buffer.append("\n" + read(new String[] { "/system/bin/cat", "/proc/version" }));
		buffer.append("\n" + "================CPU==========");
		buffer.append("\n" + read(new String[] { "/system/bin/cat", "/proc/cpuinfo" }));
		buffer.append("\n" + "================MEM==========");
		buffer.append("\n" + read(new String[] { "/system/bin/cat", "/proc/meminfo" }));
		buffer.append("\n" + "================DISK==========");
		buffer.append("\n" + read(new String[] { "/system/bin/df" }));
		buffer.append("\n！@\ue415！\ue050！\u0001f42f");
		t.setText(buffer.toString());
		sendLog();
		if (isPort) {
			portParams = buffer.toString();
		} else {
			landParams = buffer.toString();
		}

	}

	private void sendLog() {
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		String log = wm.getDefaultDisplay().getWidth() + "_" + wm.getDefaultDisplay().getHeight() + "_" + metrics.density + "_" + metrics.densityDpi + "_" + Build.VERSION.SDK_INT + "_" + getStatusHeight() + "_" + getTitleBarHeight() + "_" + Utils.getMobileModel() + "_"
				+ Build.DISPLAY + "_" + Build.VERSION.RELEASE + "_" + Build.CPU_ABI + "_" + Build.DEVICE;
		MobclickAgent.reportError(this, (isPort ? "1" : "2") + "_" + log);
	}

	private void emulateShiftHeld() {
		ClipboardManager manager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
		manager.setText(t.getText());
		Toast.makeText(this, "复制成功", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
		} else {
			isPort = false;
			t.postDelayed(new Runnable() {
				@Override
				public void run() {
					sendLog();
				}
			}, 31000);
		}
		super.onConfigurationChanged(newConfig);
		t.postDelayed(new Runnable() {
			@Override
			public void run() {
				getInfo();
			}
		}, 100);
	}

	private int getStatusHeight() {
		Rect frame = new Rect();
		getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
		return frame.top;
	}

	private int getTitleBarHeight() {
		int contentTop = getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
		return contentTop - getStatusHeight();
	}

	private String getDeviceId() {
		String android_id = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);
		return android_id;
	}

	public String read(String[] cmd) {
		StringBuffer read = new StringBuffer();
		try {
			ProcessBuilder builder = new ProcessBuilder(cmd);
			builder.directory(new File("system/bin"));
			builder.redirectErrorStream(true);
			Process process = builder.start();
			InputStream in = process.getInputStream();
			byte b[] = new byte[1024];
			int l = 0;
			while ((l = in.read(b)) != -1) {
				read.append(new String(b, 0, l));
			}
			if (in != null) {
				in.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return read.toString();
	}

	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		MobclickAgent.onKillProcess(this);
		System.exit(0);
	}

	@Override
	public void onClick(View v) {
		// int i = main.getSystemUiVisibility();
		// if (i == View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) {
		// main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
		// } else if (i == View.SYSTEM_UI_FLAG_VISIBLE) {
		// main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
		// } else if (i == View.SYSTEM_UI_FLAG_LOW_PROFILE) {
		// main.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
		// }
	}
}