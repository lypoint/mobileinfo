package com.pengpeng.mobileinfo;

import android.content.Context;
import android.content.Intent;
/**
 * 发送邮件工具
 * @author Baoshujun
 *
 */
public class EmailUtils {

  /**
   * 
   * @param context
   * @param emailReciver　邮件收件人地址
   * @param emailSubject　邮件标题
   * @param emailBody     邮件内容
   * @param filePath　 邮件附件文件路径
   */
  public static void sendEmail(Context context,String[] emailReciver,String emailSubject,String emailBody){
	   //调用系统邮件的动作Android.content.Intent.ACTION_SEND
	   Intent returnIt = new Intent(Intent.ACTION_SEND);
       
       //设置邮件收件人地址
       returnIt.putExtra(Intent.EXTRA_EMAIL, emailReciver);
       //抄送给某人
       //returnIt.putExtra(Intent.EXTRA_CC, ccs);
       //设置邮件标题
       returnIt.putExtra(Intent.EXTRA_SUBJECT, emailSubject);
       //设置邮件内容
       returnIt.putExtra(Intent.EXTRA_TEXT, emailBody);
	   returnIt.setType("message/rfc882");
	   //设置邮件客户端选择标题
	   Intent.createChooser(returnIt, "请选择邮件客户端");
	   context.startActivity(returnIt);
  }
}
