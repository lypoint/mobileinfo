package com.pengpeng.mobileinfo;

import android.util.Log;

/**
 * 这里记录调试日志
 * 
 * @author Li Hongjun
 */
public class LogInfo {

	public static void LogOut(String tag, String info) {
		Log.i(tag, info);
	}

	public static void LogOut(String info) {
		Log.i("MobileInfo", info);
	}
}
